package com.siliconaroma.plantasy;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by Durga on 2/16/2018.
 */

@GlideModule
public final class PlantasyGlideModule extends AppGlideModule {
}

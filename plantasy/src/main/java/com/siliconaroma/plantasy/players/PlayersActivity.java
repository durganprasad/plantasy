package com.siliconaroma.plantasy.players;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.SparseArray;
import android.view.View;
import android.widget.Toast;

import com.siliconaroma.plantasy.PlantasyApp;
import com.siliconaroma.plantasy.R;
import com.siliconaroma.plantasy.players.model.Player;
import com.siliconaroma.plantasy.players.model.PlayerDao;
import com.siliconaroma.plantasy.team.TeamActivity;

import java.util.ArrayList;
import java.util.List;

import static com.siliconaroma.plantasy.Constants.EXTRA_PLAYING_XI;
import static com.siliconaroma.plantasy.Constants.EXTRA_RESERVE_BENCH;
import static com.siliconaroma.plantasy.Constants.EXTRA_TEAM_BUNDLE;
import static com.siliconaroma.plantasy.Constants.PLAYER_ICONS;

/**
 * Created by Durga on 2/6/2018.
 */

public class PlayersActivity extends AppCompatActivity implements PlayersSelectionListener {
    private static final int MODE_FOR_SELECTION = 1;

    private SparseArray<Player> playingXiPlayers;
    private SparseArray<Player> reserveBenchPlayers;
    private PlayerDao playerDao;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_players);

        Toolbar toolbar = findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        final ViewPager viewPager = findViewById(R.id.players_view_pager);
        final TabLayout tabLayout = findViewById(R.id.players_tab);

        String[] playersType = getResources().getStringArray(R.array.players_type);
        for (int i = 0; i < playersType.length; i++) {
            tabLayout.addTab(tabLayout.newTab().setText(playersType[i]));
        }
        viewPager.setAdapter(new PlayersPagerAdapter(getSupportFragmentManager(), playersType));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));

        playerDao = ((PlantasyApp) getApplication()).getPlayerDbInstance();
        if (playerDao.getAllPlayers().size() <= 0) {
            playerDao.insertPlayers(getPlayersList(playersType));
        }

        playingXiPlayers = new SparseArray<>();
        for (Player player : playerDao.getPlayingXIList()) {
            playingXiPlayers.put(player.getId(), player);
        }

        reserveBenchPlayers = new SparseArray<>();
        for (Player player : playerDao.getReserveBenchList()) {
            reserveBenchPlayers.put(player.getId(), player);
        }
    }

    private List<Player> getPlayersList(String[] playersType) {
        List<Player> playerList = new ArrayList<>();

        for (int i = 1; i <= playersType.length; i++) {
            for (int j = 1; j <= 20; j++) {
                Player player = new Player(i * 100 + j, playersType[i - 1] + j, j * 3, "Team " + j % 5, i);
                player.setRuns(j * 20);
                if (i == 2) {
                    player.setWickets(j % 5);
                } else {
                    player.setWickets(0);
                }
                player.setCatches(j % 3);
                player.setInPlayingXI(false);
                player.setOnBench(false);
                player.setHasInjured(false);
                player.setPicUrl(PLAYER_ICONS[i - 1][j % 5]);
                playerList.add(player);
            }
        }
        return playerList;
    }

    @Override
    public boolean onPlayerAddedToPlayingXI(Player player) {
        int playingXiSize = playingXiPlayers.size();
        if (playingXiSize >= 11) {
            Toast.makeText(this, "You cannot add more than 11 players", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            playingXiPlayers.put(player.getId(), player);
            player.setInPlayingXI(true);
            playerDao.updatePlayer(player);
            return true;
        }
    }

    @Override
    public void onPlayerRemovedFromPlayingXI(Player player) {
        playingXiPlayers.remove(player.getId());
        player.setInPlayingXI(false);
        playerDao.updatePlayer(player);
    }

    @Override
    public void onPlayerAddedToReserveBench(Player player) {
        reserveBenchPlayers.put(player.getId(), player);
        player.setOnBench(true);
        playerDao.updatePlayer(player);
    }

    @Override
    public void onPlayerRemovedFromReserveBench(Player player) {
        reserveBenchPlayers.remove(player.getId());
        player.setOnBench(false);
        playerDao.updatePlayer(player);
    }

    public void onReviewTeam(View view) {
        if (playingXiPlayers.size() < 11) {
            Toast.makeText(this, "Please select at least 11 players for your team", Toast.LENGTH_SHORT).show();
        } else {
            Intent teamIntnt = new Intent(getApplicationContext(), TeamActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSparseParcelableArray(EXTRA_PLAYING_XI, playingXiPlayers);
            bundle.putSparseParcelableArray(EXTRA_RESERVE_BENCH, reserveBenchPlayers);
            teamIntnt.putExtra(EXTRA_TEAM_BUNDLE, bundle);
            startActivity(teamIntnt);
        }
    }
}

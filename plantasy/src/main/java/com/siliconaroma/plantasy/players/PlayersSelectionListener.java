package com.siliconaroma.plantasy.players;

import com.siliconaroma.plantasy.players.model.Player;

/**
 * Created by Durga on 2/7/2018.
 */

public interface PlayersSelectionListener {
    boolean onPlayerAddedToPlayingXI(Player player);
    void onPlayerRemovedFromPlayingXI(Player player);
    void onPlayerAddedToReserveBench(Player player);
    void onPlayerRemovedFromReserveBench(Player player);
}

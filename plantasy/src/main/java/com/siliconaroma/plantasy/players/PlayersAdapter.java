package com.siliconaroma.plantasy.players;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.TextView;

import com.siliconaroma.plantasy.R;
import com.siliconaroma.plantasy.players.model.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Durga on 1/29/2018.
 */

public class PlayersAdapter extends RecyclerView.Adapter<PlayersAdapter.PlayersViewHolder> implements Filterable {
    private Context context;
    private List<Player> playersList;
    private List<Player> filteredPlayers;
    private PlayersSelectionListener selectionListener;

    PlayersAdapter(Context context, List<Player> playersList) {
        this.context = context;
        this.playersList = playersList;
        this.filteredPlayers = playersList;
    }

    void setPlayersSelectionListener(PlayersSelectionListener listener) {
        selectionListener = listener;
    }

    @Override
    public PlayersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.players_list_item, parent, false);
        return new PlayersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PlayersViewHolder holder, int position) {
        Resources res = context.getResources();
        final Player player = filteredPlayers.get(position);
        holder.playerName.setText(player.getName());
        holder.points.setText(res.getString(R.string.player_points, player.getPoints()));
        holder.teamName.setText(res.getString(R.string.player_team_name, player.getTeam()));

        int bgResId = R.drawable.player_list_selection_options_normal;
        if (player.isInPlayingXI()) {
            bgResId = R.drawable.player_list_selection_options_selected;
        }
        holder.playingXiBtn.setBackgroundResource(bgResId);

        holder.playingXiBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (player.isInPlayingXI()) {
                    selectionListener.onPlayerRemovedFromPlayingXI(player);
                } else {
                    boolean isAdded = selectionListener.onPlayerAddedToPlayingXI(player);
                    if (isAdded && player.isOnBench()) {
                        selectionListener.onPlayerRemovedFromReserveBench(player);
                    }
                }

                notifyDataSetChanged();
            }
        });

        bgResId = R.drawable.player_list_selection_options_normal;
        if (player.isOnBench()) {
            bgResId = R.drawable.player_list_selection_options_selected;
        }
        holder.reserveBtn.setBackgroundResource(bgResId);

        holder.reserveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (player.isOnBench()) {
                    selectionListener.onPlayerRemovedFromReserveBench(player);
                } else {
                    selectionListener.onPlayerAddedToReserveBench(player);
                    if (player.isInPlayingXI()) {
                        selectionListener.onPlayerRemovedFromPlayingXI(player);
                    }
                }

                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return filteredPlayers.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String searchStr = charSequence.toString();
                if (searchStr.isEmpty()) {
                    filteredPlayers = playersList;
                } else {
                    List<Player> filteredList = new ArrayList<>();
                    for (Player player : playersList) {
                        if ((player.getName().toLowerCase().contains(searchStr.toLowerCase()))) {
                            filteredList.add(player);
                        }
                    }
                    filteredPlayers = filteredList;
                }

                FilterResults searchResults = new FilterResults();
                searchResults.values = filteredPlayers;
                return searchResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredPlayers = (List<Player>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    class PlayersViewHolder extends RecyclerView.ViewHolder {
        private TextView playerName;
        private TextView points;
        private TextView teamName;
        private ImageButton playingXiBtn;
        private ImageButton reserveBtn;

        PlayersViewHolder(View view) {
            super(view);

            playerName = view.findViewById(R.id.player_name);
            points = view.findViewById(R.id.player_points);
            teamName = view.findViewById(R.id.team_name);
            playingXiBtn = view.findViewById(R.id.playing_xi_selection);
            reserveBtn = view.findViewById(R.id.reserve_bench_selection);
        }
    }
}

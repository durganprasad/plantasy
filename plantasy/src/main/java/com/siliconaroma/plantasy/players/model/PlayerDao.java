package com.siliconaroma.plantasy.players.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * Created by Durga on 2/9/2018.
 */

@Dao
public interface PlayerDao {
    @Query("SELECT * FROM players")
    List<Player> getAllPlayers();

    @Query("SELECT * FROM players WHERE role = :role")
    List<Player> getPlayersByRole(int role);

    @Query("SELECT * FROM players WHERE is_in_playing_xi = 1")
    List<Player> getPlayingXIList();

    @Query("SELECT * FROM players WHERE is_on_bench = 1")
    Player[] getReserveBenchList();

    @Insert
    void insertPlayers(List<Player> players);

    @Update
    void updatePlayer(Player player);

    @Delete
    void deletePlayer(Player player);

    @Delete
    void deletePlayers(Player... players);
}

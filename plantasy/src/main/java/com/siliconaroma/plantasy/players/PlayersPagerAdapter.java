package com.siliconaroma.plantasy.players;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


/**
 * Created by Durga on 2/7/2018.
 */

public class PlayersPagerAdapter extends FragmentPagerAdapter {
    private String[] playersType;

    public PlayersPagerAdapter(FragmentManager fm, String[] playersType) {
        super(fm);

        this.playersType = playersType;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new PlayersFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("player_role", position + 1);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return playersType.length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return playersType[position];
    }
}

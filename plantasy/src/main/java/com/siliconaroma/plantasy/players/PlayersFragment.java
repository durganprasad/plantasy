package com.siliconaroma.plantasy.players;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.siliconaroma.plantasy.PlantasyApp;
import com.siliconaroma.plantasy.R;
import com.siliconaroma.plantasy.players.model.Player;
import com.siliconaroma.plantasy.players.model.PlayerDao;

import java.util.List;

/**
 * Created by Durga on 2/7/2018.
 */

public class PlayersFragment extends Fragment {
    private int playerRole;
    private PlayersAdapter playersAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_players, container, false);
        RecyclerView playersListView = view.findViewById(R.id.players_list);
        playersListView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        playersListView.setLayoutManager(layoutManager);

        List<Player> playerList = getPlayersList();
        if (null != playerList) {
            playersAdapter = new PlayersAdapter(getActivity(), playerList);
            playersAdapter.setPlayersSelectionListener((PlayersActivity) getActivity());
            playersListView.setAdapter(playersAdapter);
        }
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        playerRole = getArguments().getInt("player_role");
    }

    @Nullable
    private List<Player> getPlayersList() {
        PlantasyApp plantasyApp = (PlantasyApp) getActivity().getApplication();
        if (null != plantasyApp) {
            PlayerDao playerDao = plantasyApp.getPlayerDbInstance();
            return playerDao.getPlayersByRole(playerRole);
        }

        return null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_players, menu);

        MenuItem search = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) search.getActionView();
        searchPlayers(searchView);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void searchPlayers(SearchView searchView) {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                playersAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }
}

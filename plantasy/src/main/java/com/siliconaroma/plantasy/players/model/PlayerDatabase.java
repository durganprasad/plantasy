package com.siliconaroma.plantasy.players.model;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

/**
 * Created by Durga on 2/9/2018.
 */

@Database(entities = {Player.class}, version = 1)
public abstract class PlayerDatabase extends RoomDatabase {
    public abstract PlayerDao playerDao();
}

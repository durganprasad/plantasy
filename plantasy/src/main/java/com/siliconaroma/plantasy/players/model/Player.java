package com.siliconaroma.plantasy.players.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import static com.siliconaroma.plantasy.Constants.PLAYER_IMG_BASE_URL;

/**
 * Created by Durga on 2/6/2018.
 */

@Entity(tableName = "players", indices = {@Index(value = {"name", "team"}, unique = true)})
public class Player implements Parcelable {
    @PrimaryKey
    private int id;

    private String name;

    private int role;

    private int points;

    private String team;

    @ColumnInfo(name = "pic_url")
    private String picUrl;

    @ColumnInfo(name = "is_in_playing_xi")
    private boolean isInPlayingXI;

    @ColumnInfo(name = "is_on_bench")
    private boolean isOnBench;

    @ColumnInfo(name = "has_injured")
    private boolean hasInjured;

    private int runs;

    private int wickets;

    private int catches;

    public Player(int id, String name, int points, String team, int role) {
        this.id = id;
        this.name = name;
        this.points = points;
        this.team = team;
        this.role = role;
    }

    protected Player(Parcel in) {
        id = in.readInt();
        name = in.readString();
        role = in.readInt();
        points = in.readInt();
        team = in.readString();
        picUrl = in.readString();
        isInPlayingXI = in.readByte() != 0;
        isOnBench = in.readByte() != 0;
        hasInjured = in.readByte() != 0;
        runs = in.readInt();
        wickets = in.readInt();
        catches = in.readInt();
    }

    public static final Creator<Player> CREATOR = new Creator<Player>() {
        @Override
        public Player createFromParcel(Parcel in) {
            return new Player(in);
        }

        @Override
        public Player[] newArray(int size) {
            return new Player[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public int getRuns() {
        return runs;
    }

    public void setRuns(int runs) {
        this.runs = runs;
    }

    public int getWickets() {
        return wickets;
    }

    public void setWickets(int wickets) {
        this.wickets = wickets;
    }

    public int getCatches() {
        return catches;
    }

    public void setCatches(int catches) {
        this.catches = catches;
    }

    public boolean isHasInjured() {
        return hasInjured;
    }

    public void setHasInjured(boolean hasInjured) {
        this.hasInjured = hasInjured;
    }

    public boolean isInPlayingXI() {
        return isInPlayingXI;
    }

    public void setInPlayingXI(boolean inPlayingXI) {
        isInPlayingXI = inPlayingXI;
    }

    public boolean isOnBench() {
        return isOnBench;
    }

    public void setOnBench(boolean onBench) {
        isOnBench = onBench;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeInt(role);
        dest.writeInt(points);
        dest.writeString(team);
        dest.writeString(picUrl);
        dest.writeByte((byte) (isInPlayingXI ? 1 : 0));
        dest.writeByte((byte) (isOnBench ? 1 : 0));
        dest.writeByte((byte) (hasInjured ? 1 : 0));
        dest.writeInt(runs);
        dest.writeInt(wickets);
        dest.writeInt(catches);
    }
}

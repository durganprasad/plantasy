package com.siliconaroma.plantasy.series;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.siliconaroma.plantasy.R;
import com.siliconaroma.plantasy.players.PlayersActivity;
import com.siliconaroma.plantasy.series.model.Series;

import java.util.List;

/**
 * Created by Durga on 1/29/2018.
 */

public class SeriesAdapter extends RecyclerView.Adapter<SeriesAdapter.SeriesViewHolder> {
    private final int bgColors[] = {R.color.red, R.color.indigo,
            R.color.teal, R.color.yellow, R.color.blueGrey, R.color.pink,
            R.color.blue, R.color.green, R.color.orange, R.color.brown};
    private Context context;
    private List<Series> seriesList;

    public SeriesAdapter(Context context, List<Series> seriesList) {
        this.context = context;
        this.seriesList = seriesList;
    }

    @Override
    public SeriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.series_list_item, parent, false);
        SeriesViewHolder viewHolder = new SeriesViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SeriesViewHolder holder, int position) {
        Resources res = context.getResources();
        final Series series = seriesList.get(position);
        holder.seriesName.setText(series.getSeriesName());
        holder.matchCount.setText(res.getString(R.string.series_match_count, series.getMatchCount()));
        holder.teamCount.setText(res.getString(R.string.series_team_count, series.getTeamCount()));
        holder.seriesItemLyt.setBackgroundColor(res.getColor(bgColors[position % bgColors.length]));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent homeIntent = new Intent(context.getApplicationContext(), PlayersActivity.class);
                homeIntent.putExtra(Series.EXTRA_SELECTED_SERIES_ID, series.getId());
                context.startActivity(homeIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return seriesList.size();
    }

    static class SeriesViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout seriesItemLyt;
        private TextView seriesName;
        private TextView teamCount;
        private TextView matchCount;

        public SeriesViewHolder(View view) {
            super(view);

            seriesItemLyt = view.findViewById(R.id.series_item_lyt);
            seriesName = view.findViewById(R.id.series_name);
            teamCount = view.findViewById(R.id.series_team_count);
            matchCount = view.findViewById(R.id.series_match_count);
        }
    }
}

package com.siliconaroma.plantasy.series.model;

/**
 * Created by Durga on 1/29/2018.
 */

public class Series {
    public static final String EXTRA_SELECTED_SERIES_ID = "selected_series_id";

    private int id;
    private String seriesName;
    private int teamCount;
    private int matchCount;

    public int getId() {
        return id;
    }

    public String getSeriesName() {
        return seriesName;
    }

    public int getMatchCount() {
        return matchCount;
    }

    public int getTeamCount() {
        return teamCount;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSeriesName(String seriesName) {
        this.seriesName = seriesName;
    }

    public void setMatchCount(int matchCount) {
        this.matchCount = matchCount;
    }

    public void setTeamCount(int teamCount) {
        this.teamCount = teamCount;
    }
}

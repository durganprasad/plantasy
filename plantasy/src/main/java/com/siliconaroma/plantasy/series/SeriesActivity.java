package com.siliconaroma.plantasy.series;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;

import com.siliconaroma.plantasy.R;
import com.siliconaroma.plantasy.series.model.Series;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Durga on 1/22/2018.
 */

public class SeriesActivity extends AppCompatActivity {

    private RecyclerView seriesListView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_series);

        Toolbar toolbar = findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        seriesListView = findViewById(R.id.series_list);
        seriesListView.setHasFixedSize(true);
        StaggeredGridLayoutManager layoutMgr = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        seriesListView.setLayoutManager(layoutMgr);
        seriesListView.setAdapter(new SeriesAdapter(this, getSeriesList()));
    }

    private List<Series> getSeriesList() {
        List<Series> seriesList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Series series = new Series();
            series.setId(i);
            series.setSeriesName(getSeriesName(i));
            series.setTeamCount(i + 1);
            series.setMatchCount(i + 1);
            seriesList.add(series);
        }

        return seriesList;
    }

    private String getSeriesName(int position) {
        List<String> seriesNames = new ArrayList<>();
        seriesNames.add("India tour of South Africa");
        seriesNames.add("England tour of Australia");
        seriesNames.add("Triangular series of Srilanka, West Indies and Bangladesh");
        seriesNames.add("ICC U-19 Cricket World Cup");
        seriesNames.add("India tour of England, Wales and Ireland 2018");
        seriesNames.add("Pakistan tour of New Zealand");
        seriesNames.add("India Women tour of England & Wales 2018");
        seriesNames.add("Big Bash T20 League");
        seriesNames.add("India Domestic - Ranji Trophy 2018");
        seriesNames.add("Indian Premier League(IPL) 2018");
        return seriesNames.get(position);
    }
}

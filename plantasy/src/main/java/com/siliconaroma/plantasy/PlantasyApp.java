package com.siliconaroma.plantasy;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.siliconaroma.plantasy.players.model.PlayerDao;
import com.siliconaroma.plantasy.players.model.PlayerDatabase;

/**
 * Created by Durga on 2/9/2018.
 */

public class PlantasyApp extends Application {
    private PlayerDao playerDao;

    @Override
    public void onCreate() {
        super.onCreate();

        PlayerDatabase db = Room.databaseBuilder(getApplicationContext(),
                PlayerDatabase.class, "plantasy.db").allowMainThreadQueries().build();
        playerDao = db.playerDao();
    }

    public PlayerDao getPlayerDbInstance() {
        return playerDao;
    }
}

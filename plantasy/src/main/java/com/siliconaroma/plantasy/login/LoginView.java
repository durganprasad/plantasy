package com.siliconaroma.plantasy.login;

/**
 * Created by NallamNP on 12/28/2017.
 */

public interface LoginView {
    void showProgress();

    void hideProgress();

    void setUsernameError();

    void setPasswordError();

    void navigateToHome();
}

package com.siliconaroma.plantasy.login;

import android.text.TextUtils;

/**
 * Created by NallamNP on 12/28/2017.
 */

public class LoginInteractorImpl implements LoginInteractor {

    @Override
    public void login(String username, String password, OnLoginFinishedListener listener) {
        if (TextUtils.isEmpty(username)) {
            listener.onUsernameError();
        } else if (TextUtils.isEmpty(password)) {
            listener.onPasswordError();
        } else {
            listener.onSuccess();
        }
    }
}

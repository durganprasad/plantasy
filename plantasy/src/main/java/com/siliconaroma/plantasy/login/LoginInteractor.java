package com.siliconaroma.plantasy.login;

/**
 * Created by NallamNP on 12/28/2017.
 */

public interface LoginInteractor {
    interface OnLoginFinishedListener {
        void onUsernameError();

        void onPasswordError();

        void onSuccess();
    }

    void login(String username, String password, OnLoginFinishedListener listener);
}

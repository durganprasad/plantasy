package com.siliconaroma.plantasy.login;

/**
 * Created by NallamNP on 12/28/2017.
 */

public interface LoginPresenter {
    void validateCredentials(String username, String password);

    void onDestroy();
}

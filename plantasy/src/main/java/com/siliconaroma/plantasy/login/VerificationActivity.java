package com.siliconaroma.plantasy.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.support.v7.widget.Toolbar;

import com.siliconaroma.plantasy.R;
import com.siliconaroma.plantasy.series.SeriesActivity;

/**
 * Created by Durga on 1/11/2018.
 */

public class VerificationActivity extends AppCompatActivity {
    private EditText verificationCodeEdtr;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_verification);

        Toolbar toolbar = findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        verificationCodeEdtr = findViewById(R.id.verification_code_edtr);
        verificationCodeEdtr.addTextChangedListener(verificationCodeChangeListener);
    }

    private TextWatcher verificationCodeChangeListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            int length = editable.length();
            if (length == 6) {
                launchSeriesScreen();
            }
        }
    };

    private void launchSeriesScreen() {
        startActivity(new Intent(getApplicationContext(), SeriesActivity.class));
    }
}

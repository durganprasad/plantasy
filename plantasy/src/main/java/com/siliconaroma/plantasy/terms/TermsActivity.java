package com.siliconaroma.plantasy.terms;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toolbar;

import com.siliconaroma.plantasy.R;
import com.siliconaroma.plantasy.login.LoginActivity;
import com.siliconaroma.plantasy.model.AppModel;
import com.siliconaroma.plantasy.series.SeriesActivity;

/**
 * Created by Durga on 1/8/2018.
 */

public class TermsActivity extends AppCompatActivity {

    private Context appContext;
    private AppModel appModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appContext = getApplicationContext();
        appModel = new AppModel(appContext);

        if (appModel.hasUserAgreedTerms()) {
            launchHomeScreen();
        } else {
            setContentView(R.layout.activity_terms);
        }
    }

    public void onAgreeTerms(View view) {
        appModel.saveUserHasAgreedTerms(true);
        startActivity(new Intent(appContext, LoginActivity.class));
    }

    private void launchHomeScreen() {
        startActivity(new Intent(appContext, SeriesActivity.class));
    }
}

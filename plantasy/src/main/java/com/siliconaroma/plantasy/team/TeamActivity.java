package com.siliconaroma.plantasy.team;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.SparseArray;
import android.view.View;

import com.siliconaroma.plantasy.R;
import com.siliconaroma.plantasy.home.HomeActivity;
import com.siliconaroma.plantasy.players.model.Player;

import static com.siliconaroma.plantasy.Constants.EXTRA_PLAYING_XI;
import static com.siliconaroma.plantasy.Constants.EXTRA_RESERVE_BENCH;
import static com.siliconaroma.plantasy.Constants.EXTRA_TEAM_BUNDLE;

/**
 * Created by Durga on 2/12/2018.
 */

public class TeamActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_team);

        Toolbar toolbar = findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        Bundle bundle = getIntent().getBundleExtra(EXTRA_TEAM_BUNDLE);
        SparseArray<Player> playingXi = bundle.getSparseParcelableArray(EXTRA_PLAYING_XI);
        SparseArray<Player> reserveBench = bundle.getSparseParcelableArray(EXTRA_RESERVE_BENCH);

        final ViewPager viewPager = findViewById(R.id.players_view_pager);
        final TabLayout tabLayout = findViewById(R.id.players_tab);

        String[] playerGroups = getResources().getStringArray(R.array.team_player_groups);
        for (int i = 0; i < playerGroups.length; i++) {
            tabLayout.addTab(tabLayout.newTab().setText(playerGroups[i]));
        }

        TeamPagerAdapter adapter = new TeamPagerAdapter(getSupportFragmentManager(), playerGroups);
        adapter.setPlayingXiTeam(playingXi);
        adapter.setReserveBenchTeam(reserveBench);
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));
    }

    public void onConfirm(View view) {
        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
    }
}

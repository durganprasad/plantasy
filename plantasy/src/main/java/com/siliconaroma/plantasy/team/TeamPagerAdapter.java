package com.siliconaroma.plantasy.team;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;

import com.siliconaroma.plantasy.Constants;
import com.siliconaroma.plantasy.players.model.Player;

/**
 * Created by Durga on 2/7/2018.
 */

public class TeamPagerAdapter extends FragmentPagerAdapter {
    private String[] playerGroups;
    private SparseArray<Player> playingXi;
    SparseArray<Player> reserveBench;

    public TeamPagerAdapter(FragmentManager fm, String[] playerGroups) {
        super(fm);

        this.playerGroups = playerGroups;
    }

    public void setPlayingXiTeam(SparseArray<Player> playingXi) {
        this.playingXi = playingXi;
    }

    public void setReserveBenchTeam(SparseArray<Player> reserveBench) {
        this.reserveBench = reserveBench;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Fragment fragment = new PlayingXiTeamFragment();
                Bundle bundle = new Bundle();
                bundle.putSparseParcelableArray(Constants.EXTRA_PLAYING_XI, playingXi);
                fragment.setArguments(bundle);
                return fragment;

            case 1:
                fragment = new ReserveBenchTeamFragment();
                bundle = new Bundle();
                bundle.putSparseParcelableArray(Constants.EXTRA_RESERVE_BENCH, reserveBench);
                fragment.setArguments(bundle);
                return fragment;
        }

        return null;
    }

    @Override
    public int getCount() {
        return playerGroups.length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return playerGroups[position];
    }
}

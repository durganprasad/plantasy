package com.siliconaroma.plantasy.team;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.siliconaroma.plantasy.Constants;
import com.siliconaroma.plantasy.GlideApp;
import com.siliconaroma.plantasy.R;
import com.siliconaroma.plantasy.players.model.Player;

import static com.siliconaroma.plantasy.Constants.PLAYER_IMG_BASE_URL;

/**
 * Created by Durga on 2/14/2018.
 */

public class PlayingXiTeamFragment extends Fragment {
    private SparseArray<Player> playingXi;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        playingXi = getArguments().getSparseParcelableArray(Constants.EXTRA_PLAYING_XI);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_playing_xi_team, container, false);
        bindPlayersData(view);
        return view;
    }

    private void bindPlayersData(View view) {
        int[] playerImgViews = {R.id.captain_img, R.id.player1_img,
                R.id.player2_img, R.id.player3_img, R.id.player4_img,
                R.id.player5_img, R.id.player6_img, R.id.player7_img,
                R.id.player8_img, R.id.player9_img, R.id.player10_img};

        for (int i = 0; i < 11; i++) {
            ImageView playerImg = view.findViewById(playerImgViews[i]);
            Player player = playingXi.valueAt(i);
            ImageView imgView = view.findViewById(playerImgViews[i]);
            GlideApp.with(this).load(PLAYER_IMG_BASE_URL + player.getPicUrl())
                    .placeholder(R.drawable.player_icon).circleCrop().into(imgView);
        }
    }
}

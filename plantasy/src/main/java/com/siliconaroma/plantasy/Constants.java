package com.siliconaroma.plantasy;

/**
 * Created by Durga on 2/12/2018.
 */

public interface Constants {
    String EXTRA_PLAYING_XI = "playing_xi";
    String EXTRA_RESERVE_BENCH = "reserve_bench";
    String EXTRA_TEAM_BUNDLE = "team";
    String PLAYER_IMG_BASE_URL = "http://i.cricketcb.com/stats/img/faceImages/";

    String[][] PLAYER_ICONS = {
            {"1413.jpg", "370.jpg", "25.jpg", "26.jpg", "247.jpg"},
            {"1726.jpg", "9311.jpg", "1454.jpg", "71.jpg", "269.jpg"},
            {"265.jpg", "145.jpg", "10744.jpg", "8271.jpg", "7884.jpg"},
            {"1593.jpg", "587.jpg", "9647.jpg", "69.jpg", "6557.jpg"}
    };
}
package com.siliconaroma.plantasy.model;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Durga on 2/28/2018.
 */

public interface PlantasyApiClient {
    @GET("/orgs/{user}/repos")
    Call<List<GitHubRepo>> reposForUser(@Path("user") String user);
}

package com.siliconaroma.plantasy.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Durga on 1/11/2018.
 */

public class AppModel {
    private static final String PREF_KEY_AGREE_TERMS = "hasAgreedTerms";
    private static final String PREF_KEY_MOBILE = "mobileNumber";
    private static final String PREF_KEY_SELECTED_SERIES = "selectedSeries";

    private SharedPreferences prefs;
    private SharedPreferences.Editor prefsEditor;

    public AppModel(Context context) {
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefsEditor = prefs.edit();
    }

    public void saveUserHasAgreedTerms(boolean hasAgreed) {
        prefsEditor.putBoolean(PREF_KEY_AGREE_TERMS, hasAgreed);
        prefsEditor.apply();
    }

    public boolean hasUserAgreedTerms() {
        return prefs.getBoolean(PREF_KEY_AGREE_TERMS, false);
    }

    public void saveUserMobileNumber(String mobileNumber) {
        prefsEditor.putString(PREF_KEY_MOBILE, mobileNumber);
        prefsEditor.apply();
    }

    public String getUserMobileNumber() {
        return prefs.getString(PREF_KEY_MOBILE, null);
    }

    public void saveUserSelectedSeries(String series) {
        prefsEditor.putString(PREF_KEY_SELECTED_SERIES, series);
        prefsEditor.apply();
    }

    public String getUserSelectedSeries() {
        return prefs.getString(PREF_KEY_SELECTED_SERIES, null);
    }
}
